# Sample 3tier app

This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.

# How the Pipeline Works

The pipeline picks up changes at specific dirs and runs specific Jobs. Specific tiers can be runned by using the RUN_TIER environment variable. It will besically invoke that tier only.

- The Pipeline uses a specially built docker image. It will be pushed to gitlab registory and will be used to run the all jobs. You can always manually invoke building new by running the pipeline with `BUILD_CI_IMAGE` set to `true`).
- Architecture Diagram, found in `Toptal 3Tier Node.drawio`

## Features

- GitLab [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/) Integration
- Terraform Linting
- Terraform Static Security analysis
- Terraform Reports Presentation in MRs
- Other Unit Test Reports Artificats
- Terraform Validation and Format checks
- Code in all Tiers testing by calling `npm test`
- Build and deployment of Images of all Tiers to ECR

## How to Run

**Notes**

- The current setup uses `eu-north-1` to deploy infrastructure. You can aways change that in Terraform vars.
- Make sure to create the remote s3 bucket with [appropriate permissions](https://www.terraform.io/language/settings/backends/s3#s3-bucket-permissions).
- You can always deploy specific image tags by setting the specific env variable for the env, for that tier.
  - **web_image_tag**: Web Image Tag
  - **api_image_tag**: Api Image Tag

You will need the following CI variables set up

- **ECR_REGISTRY_DEV_URL**: Registory Address to push DEV ECR images. It will be created by Teraform (Hint: Use RUN_TIER = 'infra' to run Terraform only)

### AWS Authentication

- **AWS_ACCESS_KEY_ID**: Access Key to use for deployments
- **AWS_SECRET_ACCESS_KEY**: Access Secret for use for deployments
