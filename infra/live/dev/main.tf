terraform {
  backend "s3" {
    bucket = "mernxl--terraform-states"
    key    = "total-infra-dev"
    region = "eu-north-1"
  }
}

provider "aws" {
  region = var.aws_region

  default_tags {
    tags = {
      Environment = var.environment
    }
  }
}

module "networking" {
  source = "../../modules/networking"

  environment = var.environment
  aws_region  = var.aws_region
  vpc_cidr    = var.vpc_cidr
}

module "rds" {
  source = "../../modules/rds"

  environment = var.environment
  vpc_id      = module.networking.vpc_id
  Tier        = "db"

  db_engine_version = "13.4"
  depends_on        = [module.networking]
}

module "ecs" {
  source = "../../modules/ecs"

  environment = var.environment
  vpc_id      = module.networking.vpc_id


  web_image_tag = var.web_image_tag
  api_image_tag = var.api_image_tag

  db_creds = {
    db_host = module.rds.db_host
    db_port = module.rds.db_port
    db_name = module.rds.db_name
    db_user = module.rds.db_user
  }
  db_pass_secret_arn = module.rds.db_pass_secret_arn

  depends_on = [module.networking, module.rds]
} 