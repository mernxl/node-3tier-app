variable "aws_region" {
  type        = string
  description = "Region to lunch Infrastructure in"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR to use"
}

variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "web_image_tag" {
  type        = string
  description = "Tag to deploy on web"
}

variable "api_image_tag" {
  type        = string
  description = "Tag to deploy on api"
}