variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to launch the ecs in"
}

variable "Tier" {
  type        = string
  description = "Module's Tier"
}

variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "app_port" {
  type        = number
  description = "Application port, what port to listen on"
}

variable "health_check_path" {
  type        = string
  default     = "/"
  description = "Path to check for target health"
}