variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to launch the ecs in"
}

variable "Tier" {
  type        = string
  description = "Module's Tier"
}

variable "cluster_id" {
  type        = string
  description = "Cluster ID to launch the ecs in"
}

variable "cluster_name" {
  type        = string
  description = "Cluster Name of our working cluster"
}

variable "alb_arn_suffix" {
  type        = string
  description = "ALB target group arn suffix for the service"
}

variable "alb_target_group_arn" {
  type        = string
  description = "ALB target group for the service"
}

variable "alb_target_group_arn_suffix" {
  type        = string
  description = "ALB target group suffix for the service"
}

variable "autoscaling_settings" {
  type = object({
    min_capacity : number
    max_capacity : number
    target_request_value : number
  })
  description = "Autoscaling settings"
}

variable "app_name" {
  type        = string
  description = "A unique name to our application"
}

variable "app_port" {
  type        = number
  description = "Application port, what port to listen on"
}

variable "image_ecr_url" {
  type        = string
  description = "ECR URL of our image"
}

variable "image_tag" {
  type        = string
  description = "Image Tag to get"
}

variable "fargate_cpu" {
  type        = number
  default     = 256 # 0.25 vCPU
  description = "Container memory demand"
}

variable "fargate_mem" {
  type        = number
  default     = 512
  description = "Container memory demands"
}

variable "container_cpu" {
  type        = number
  default     = 256 # 0.25 vCPU
  description = "Container memory demands"
}

variable "container_mem" {
  type        = number
  default     = 256
  description = "Container memory demands"
}

variable "task_environment_variables" {
  type = list(object({
    name  = string
    value = string
  }))
  default     = []
  description = "Environment Variables for our tasks"
}

variable "task_secret_environment_variables" {
  type = list(object({
    name      = string
    valueFrom = string
  }))
  default     = []
  description = "From secrets arn, to Environment Variables for our tasks"
}

