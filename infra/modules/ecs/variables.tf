variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to launch the ecs in"
}

variable "web_image_tag" {
  type        = string
  description = "Tag to deploy on web"
}

variable "api_image_tag" {
  type        = string
  description = "Tag to deploy on api"
}

variable "db_creds" {
  type = object({
    db_host : string
    db_port : string
    db_name : string
    db_user : string
  })
  description = "DB credentials for api"
}

variable "db_pass_secret_arn" {
  type        = string
  description = "ARN of the DB secrets to be used by api"
} 