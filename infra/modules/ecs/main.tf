resource "aws_ecs_cluster" "main" {
  name = "main-cluster-${var.environment}"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "fargate" {
  cluster_name = aws_ecs_cluster.main.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

module "web-alb" {
  source = "./alb"

  environment = var.environment
  Tier        = "web"
  vpc_id      = var.vpc_id
  app_name    = "web"
  app_port    = 8080
}

module "api-alb" {
  source = "./alb"

  environment       = var.environment
  Tier              = "api"
  vpc_id            = var.vpc_id
  app_name          = "api"
  app_port          = 8080
  health_check_path = "/api/status"
}

module "web-cloudfront" {
  count   = 0 # Could not provision, account not verified for use with CloudFront
  source  = "terraform-aws-modules/cloudfront/aws"
  version = "~> 2.9"

  price_class = "PriceClass_200" # Regional Distribution
  comment     = "Cloudfront for Web distribution"

  origin = {
    web_alb = {
      domain_name = module.api-alb.lb_dns_name
      origin_id   = module.api-alb.lb_id
    }
  }

  default_cache_behavior = {
    target_origin_id       = module.web-alb.lb_id
    viewer_protocol_policy = "allow-all"

    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
    query_string    = true
  }

  tags = {
    Tier = "web"
  }

  depends_on = [module.web-alb]
}


#tfsec:ignore:aws-ecr-repository-customer-key
#tfsec:ignore:aws-ecr-enforce-immutable-repository
resource "aws_ecr_repository" "web" {
  name                 = "main-web-${var.environment}"
  image_tag_mutability = "MUTABLE" # allow so we can always set latest

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    Tier = "web"
  }
}

#tfsec:ignore:aws-ecr-repository-customer-key
#tfsec:ignore:aws-ecr-enforce-immutable-repository
resource "aws_ecr_repository" "api" {
  name                 = "main-api-${var.environment}"
  image_tag_mutability = "MUTABLE" # allow so we can always set latest

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    Tier = "api"
  }
}

module "web-ecs" {
  source = "./service"

  environment = var.environment
  Tier        = "web"

  cluster_id    = aws_ecs_cluster.main.id
  cluster_name  = aws_ecs_cluster.main.name
  vpc_id        = var.vpc_id
  app_name      = "web"
  app_port      = 8080
  image_ecr_url = aws_ecr_repository.web.repository_url
  image_tag     = var.web_image_tag

  autoscaling_settings = {
    min_capacity         = 1
    max_capacity         = 2
    target_request_value = 100
  }
  alb_arn_suffix              = module.web-alb.lb_arn_suffix
  alb_target_group_arn        = module.web-alb.lb_target_group_arns[0]
  alb_target_group_arn_suffix = module.web-alb.lb_target_group_arn_suffixes[0]

  task_environment_variables = [
    { name = "API_HOST", value = "http://${module.api-alb.lb_dns_name}" }
  ]

  depends_on = [module.web-alb, module.api-alb]
}

module "api-ecs" {
  source = "./service"

  environment = var.environment
  Tier        = "api"

  cluster_id    = aws_ecs_cluster.main.id
  cluster_name  = aws_ecs_cluster.main.name
  vpc_id        = var.vpc_id
  app_name      = "api"
  app_port      = 8080
  image_ecr_url = aws_ecr_repository.api.repository_url
  image_tag     = var.api_image_tag

  autoscaling_settings = {
    min_capacity         = 1
    max_capacity         = 2
    target_request_value = 100
  }
  alb_arn_suffix              = module.api-alb.lb_arn_suffix
  alb_target_group_arn        = module.api-alb.lb_target_group_arns[0]
  alb_target_group_arn_suffix = module.api-alb.lb_target_group_arn_suffixes[0]

  task_environment_variables = [
    { name : "DB", value : var.db_creds.db_name },
    { name : "DBUSER", value : var.db_creds.db_user },
    { name : "DBHOST", value : var.db_creds.db_host },
    { name : "DBPORT", value : tostring(var.db_creds.db_port) }
  ]

  task_secret_environment_variables = [
    { name = "DBPASS", valueFrom = var.db_pass_secret_arn }
  ]

  depends_on = [module.api-alb]
}
