locals {
  azs = [for name in slice(data.aws_availability_zones.available.names, 1, var.az_count + 1) :
    { name = name, short = substr(name, -2, -1) }
  ]
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "main-vpc-${var.environment}"
  }
}

resource "aws_subnet" "web" {
  count = length(local.azs)

  vpc_id            = aws_vpc.main.id
  availability_zone = local.azs[count.index].name
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index)

  tags = {
    Name = "web-${local.azs[count.index].short}-${var.environment}"
    Tier = "web"
  }
}

resource "aws_subnet" "api" {
  count = length(local.azs)

  vpc_id            = aws_vpc.main.id
  availability_zone = local.azs[count.index].name
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index + length(local.azs))

  tags = {
    Name = "api-${local.azs[count.index].short}-${var.environment}"
    Tier = "api"
  }
}

resource "aws_subnet" "db" {
  count = length(local.azs)

  vpc_id            = aws_vpc.main.id
  availability_zone = local.azs[count.index].name
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index + 2 * length(local.azs))

  tags = {
    Name = "db-${local.azs[count.index].short}-${var.environment}"
    Tier = "db"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main-igw-${var.environment}"
  }
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main-rt-${var.environment}"
  }
}

resource "aws_route" "igw" {
  route_table_id         = aws_route_table.main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_route_table_association" "web" {
  count          = length(aws_subnet.web)
  subnet_id      = aws_subnet.web[count.index].id
  route_table_id = aws_route_table.main.id
}

resource "aws_route_table_association" "api" {
  count          = length(aws_subnet.api)
  subnet_id      = aws_subnet.api[count.index].id
  route_table_id = aws_route_table.main.id
}
