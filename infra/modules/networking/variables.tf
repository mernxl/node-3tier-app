variable "aws_region" {
  type        = string
  description = "Region to lunch Infrastructure in"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR to use"
}

variable "environment" {
  type        = string
  description = "Specify Environment, to be used in naming resources for an environment"
}

variable "az_count" {
  type        = number
  default     = 2
  description = "Number of availability zones to use"
}
