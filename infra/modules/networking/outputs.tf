output "vpc_id" {
  value       = aws_vpc.main.id
  description = "ID of the created VPC"
}
