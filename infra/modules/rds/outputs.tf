output "db_host" {
  value       = module.main_db.db_instance_address
  description = "Secrets arn, can be used by some other module"
}

output "db_port" {
  value       = var.pg_port
  description = "Secrets arn, can be used by some other module"
}

output "db_name" {
  value       = local.db_name
  description = "Secrets arn, can be used by some other module"
}

output "db_user" {
  value       = local.username
  description = "Secrets arn, can be used by some other module"
}

output "db_pass_secret_arn" {
  value       = aws_secretsmanager_secret.password.arn
  description = "Secrets arn, can be used by some other module"
}
