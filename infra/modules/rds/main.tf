resource "random_password" "root_password" {
  length  = 32
  special = false
}

locals {
  db_name  = "maindb"
  username = "root"
  password = random_password.root_password.result
}

resource "aws_security_group" "allow_postgres" {
  name        = "terraform-managed-postgres-${var.environment}"
  description = "PostgreSQL Traffic"
  vpc_id      = data.aws_vpc.main.id

  ingress {
    description = "PostgreSQL access from within VPC"
    from_port   = var.pg_port
    to_port     = var.pg_port
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.main.cidr_block]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [data.aws_vpc.main.cidr_block]
  }

  tags = {
    Name = "terraform-managed-postgres-${var.environment}"
    Tier = var.Tier
  }
}

module "main_db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 4.1"

  identifier = "main-db-${var.environment}"

  engine            = var.db_engine
  engine_version    = var.db_engine_version
  instance_class    = var.db_instance_class
  allocated_storage = 5

  db_name  = local.db_name
  port     = var.pg_port
  username = local.username
  password = local.password

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [aws_security_group.allow_postgres.id]

  maintenance_window      = "Mon:00:00-Mon:03:00"
  backup_window           = "03:00-06:00"
  backup_retention_period = 10 # days

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  monitoring_interval             = "30"
  monitoring_role_name            = "terraform-managed-rds-monitoring-role-${var.environment}"
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  create_monitoring_role          = true

  performance_insights_enabled = true

  tags = {
    Tier = var.Tier
  }

  # DB subnet group
  multi_az               = false
  create_db_subnet_group = true
  db_subnet_group_tags = {
    Tier = var.Tier
  }
  subnet_ids = data.aws_subnets.db.ids


  # Database Deletion Protection
  deletion_protection = true

  create_db_parameter_group = false
  create_db_option_group    = false
}

#tfsec:ignore:aws-ssm-secret-use-customer-key
resource "aws_secretsmanager_secret" "password" {
  name = "terraform-managed-maindb-password-${var.environment}"

  tags = {
    Tier = var.Tier
  }
}

resource "aws_secretsmanager_secret_version" "password" {
  secret_id     = aws_secretsmanager_secret.password.id
  secret_string = local.password
}
