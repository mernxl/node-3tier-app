data "aws_vpc" "main" {
  id = var.vpc_id
}

data "aws_subnets" "db" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  tags = {
    Tier = var.Tier
  }
}
