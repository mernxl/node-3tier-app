include:
  - template: Security/SAST.gitlab-ci.yml

workflow:
  rules:
    # do not run for merge requests
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: "$CI_COMMIT_BRANCH"

variables:
  # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"
  CI_IMAGE_TAG: "latest"

stages:
  - pre
  - check
  - test
  - check-build
  - build

  - plan
  - apply

# Environment
.api-dev:
  {
    variables:
      {
        ENV_NAME: dev,
        TIER: api,
        AWS_REGION: eu-north-1,
        ECR_REGISTRY_URL: $ECR_REGISTRY_DEV_URL,
      },
  }
.web-dev:
  {
    variables:
      {
        ENV_NAME: dev,
        TIER: web,
        AWS_REGION: eu-north-1,
        ECR_REGISTRY_URL: $ECR_REGISTRY_DEV_URL,
      },
  }

.infra-dev: { variables: { ENV_NAME: dev, TIER: infra } }

image: $CI_REGISTRY_IMAGE:$CI_IMAGE_TAG

# not when from pipeline
.master: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"

.infra-changedfiles:
  - .gitlab-ci.yml
  - .gitignore
  - Dockerfile
  - infra/modules/**/*
  - infra/live/${ENV_NAME}/**/*

.changedfiles:
  - .gitlab-ci.yml
  - .gitignore
  - Dockerfile
  - ${TIER}/**/*

.build-image-base:
  services:
    - docker:dind
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  before_script:
    - amazon-linux-extras install docker
    - aws --version
    - docker --version

build-upload-ci:
  extends: [.build-image-base]
  stage: pre
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_PIPELINE_IID
    IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
    - echo $IMAGE_TAG
    - docker tag $IMAGE_TAG $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST
  rules:
    - if: $BUILD_CI_IMAGE && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH # if true will build the CI Image
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - Dockerfile

.check:
  stage: check
  rules:
    - if: $RUN_TIER == $TIER # from console
    - if: "$CI_COMMIT_BRANCH"
      changes: !reference [.infra-changedfiles]

.format-base:
  extends: [.check]
  script:
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform fmt -diff -recursive -check

.validate-base:
  extends: [.check]
  script:
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform init -backend=false -force-copy -reconfigure
    - terraform validate

.tflint-base:
  extends: [.check]
  script:
    - tflint --version
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform init -backend=false -force-copy -reconfigure
    - tflint --format=junit  --var-file=terraform.tfvars . > tflint.xml
  artifacts:
    reports:
      junit:
        - $TIER/live/$ENV_NAME/tflint.xml

.tfsec-base:
  extends: [.check]
  script:
    - tfsec --version
    - cd $TIER/live/$ENV_NAME
    - tfswitch
    - terraform init -backend=false -force-copy -reconfigure
    - tfsec --format=default,junit --tfvars-file=terraform.tfvars -O tfsec .
  artifacts:
    reports:
      junit:
        - $TIER/live/$ENV_NAME/tfsec.junit

.test-base:
  stage: test
  script:
    - cd $TIER
    - npm install
    - npm run test
  rules:
    - if: $RUN_TIER == $TIER
    - if: "$CI_COMMIT_BRANCH"
      changes: !reference [.changedfiles]

.build-tier-image:
  extends: [.build-image-base]
  variables:
    IMAGE_TAG: $ECR_REGISTRY_URL/main-$TIER-$ENV_NAME:$CI_PIPELINE_IID
    IMAGE_TAG_LATEST: $ECR_REGISTRY_URL/main-$TIER-$ENV_NAME
  script:
    - cd $TIER
    - docker build -t $IMAGE_TAG .

.check-build-base:
  extends: [.build-tier-image]
  stage: check-build
  rules:
    - if: ($CI_COMMIT_BRANCH || $RUN_TIER == $TIER) && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes: !reference [.changedfiles]

.build-upload-base:
  extends: [.build-tier-image]
  stage: build
  script:
    - !reference [.build-tier-image, script]
    - aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $ECR_REGISTRY_URL
    - docker push $IMAGE_TAG
    - docker tag $IMAGE_TAG $IMAGE_TAG_LATEST
    - docker push $IMAGE_TAG_LATEST
  rules:
    - if: $RUN_TIER == $TIER && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: !reference [.changedfiles]

.tf-base:
  variables:
    PLAN: plan.cache
    PLAN_JSON: plan.json
    TF_ENV_DIR: $TIER/live/$ENV_NAME
  before_script:
    - cd $TF_ENV_DIR
    - tfswitch
    - terraform init

.plan-base:
  extends: [.tf-base]
  stage: plan
  script:
    - terraform plan -out=$PLAN
    - terraform show --json $PLAN | jq -r '([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}' > $PLAN_JSON
  artifacts:
    paths:
      - $TF_ENV_DIR/$PLAN
    reports:
      terraform: $TF_ENV_DIR/$PLAN_JSON
  rules:
    - if: $RUN_TIER == $TIER # from console
    - if: "$CI_COMMIT_BRANCH"
      changes: !reference [.infra-changedfiles]

.apply-base:
  extends: [.tf-base]
  stage: apply
  script:
    - terraform apply $PLAN
  rules:
    - if: $RUN_TIER == $TIER # from console
      when: manual
    - if: "$CI_DEFAULT_BRANCH"
      when: manual
      changes: !reference [.infra-changedfiles]

format-infra-dev: { extends: [.infra-dev, .format-base] }
validate-infra-dev: { extends: [.infra-dev, .validate-base] }
tflint-infra-dev: { extends: [.infra-dev, .tflint-base] }
tfsec-infra-dev: { extends: [.infra-dev, .tfsec-base] }
plan-infra-dev: { extends: [.infra-dev, .plan-base] }
apply-infra-dev:
  { extends: [.infra-dev, .apply-base], dependencies: [plan-infra-dev] }

test-api-dev: { extends: [.api-dev, .test-base] }
check-build-api-dev: { extends: [.api-dev, .check-build-base] }
build-upload-api-dev: { extends: [.api-dev, .build-upload-base] }

test-web-dev: { extends: [.web-dev, .test-base] }
check-build-web-dev: { extends: [.web-dev, .check-build-base] }
build-upload-web-dev: { extends: [.web-dev, .build-upload-base] }
